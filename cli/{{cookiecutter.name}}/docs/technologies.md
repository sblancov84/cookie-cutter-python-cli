# Used technologies

## Developing

* [Python](https://www.python.org/)

## Analysing code statically

* [Flake8](https://gitlab.com/pycqa/flake8)
* [Prospector](http://prospector.landscape.io/en/master/)
* [Hadolint](https://github.com/hadolint/hadolint)

## Testing code

* [Pytest](https://docs.pytest.org/en/stable/)
* [Coverage](https://coverage.readthedocs.io/)

## Isolating environments

* [Docker](https://www.docker.com/)
* [pyenv](https://github.com/pyenv/pyenv)
* [autoenv](https://github.com/inishchith/autoenv)

## Storing credentials

* [Pass](https://www.passwordstore.org/)

## Help

* [Make](https://www.gnu.org/software/make/manual/make.html)


## Docker

* [Docker](https://docker.com/)
* [Hadolint](https://github.com/hadolint/hadolint)

Too know what is every code of hadolint consult
[its wiki](https://github.com/hadolint/hadolint/wiki)
