#!/usr/bin/env bash

function printnl() {
    printf "$1\n"
}

function check_so_dependency () {
    COMMAND=$1
    TOOL_NAME=$2
    if [ $# -lt 2 ]
    then
        TOOL_NAME=$1
    fi

    which $COMMAND > /dev/null
    if [ $? -eq 0 ]
    then
        printnl "ok $TOOL_NAME was found"
    else
        printnl ":( $TOOL_NAME was NOT found"
    fi
}

check_so_dependency "make"
check_so_dependency "docker"
check_so_dependency "pyenv"
check_so_dependency "autoenv_init" "autoenv"
