from {{cookiecutter.name}}.cli import Version
from {{cookiecutter.name}}.arguments.arguments import Argument


class TestExample:

    def test_something_fails(self):
        """This test should be fixed or deleted"""
        assert not issubclass(Version, Argument)
