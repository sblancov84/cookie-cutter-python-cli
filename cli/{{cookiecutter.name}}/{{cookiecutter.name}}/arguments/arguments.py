from {{cookiecutter.name}}.arguments.base import ArgumentBase


class Argument(type, ArgumentBase):
    """It allows to register Argument classes into Arguments automatically"""

    def __init__(klass, name, bases, clsdict):
        Arguments.register(klass())
        super().__init__(name, bases, clsdict)


class Arguments:
    """Argument collection"""

    arguments = []

    def __init__(self, parser):
        self.parser = parser

    def define(self):
        for argument in Arguments.arguments:
            names, options = argument.define()
            self.parser.add_argument(*names, **options)

    def execute(self):
        args = self.parser.parse_args()
        for argument in self.arguments:
            argument.execute(args)

    @classmethod
    def register(self, klass):
        Arguments.arguments.append(klass)
