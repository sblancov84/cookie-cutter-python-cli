from abc import ABC, abstractmethod


class ArgumentBase(ABC):

    @abstractmethod
    def define(self):
        pass

    @abstractmethod
    def execute(self, args):
        pass
