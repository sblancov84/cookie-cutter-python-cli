import argparse

from {{cookiecutter.name}} import __version__
from {{cookiecutter.name}}.arguments.arguments import Argument, Arguments


class Cli:
    """Command Line Interface"""

    def __init__(self):
        parser = argparse.ArgumentParser(prog="{{cookiecutter.name}}")

        arguments = Arguments(parser)
        arguments.define()
        arguments.execute()


class Version(metaclass=Argument):
    """Show current version"""

    def define(self):
        options = {
            "action": "version",
            "version": f"%(prog)s {__version__}"
        }
        arguments = ["-V", "--version"]
        return arguments, options

    def execute(self, args):
        pass
