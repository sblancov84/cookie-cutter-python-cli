from {{cookiecutter.name}}.cli import Cli


def main():
    Cli()


if __name__ == "__main__":
    main()
